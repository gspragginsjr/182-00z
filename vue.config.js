// const path = require('path');
// const PrerenderSpaPlugin = require('prerender-spa-plugin');

module.exports = {
  configureWebpack() {
    const config = {};

    // if (process.env.NODE_ENV === 'production') {
    //   config.plugins = [
    //     new PrerenderSpaPlugin({
    //       staticDir: path.join(__dirname, 'dist'),
    //       routes: ['/'],
    //       minify: {
    //         removeComments: true,
    //         collapseWhitespace: true,
    //         removeAttributeQuotes: true,
    //         collapseBooleanAttributes: true,
    //         removeScriptTypeAttributes: true,
    //       },
    //     }),
    //   ];
    // }

    config.module = {
      rules: [
        {
          test: /\.yml$/,
          loader: 'json-loader!yaml-loader',
        },
      ],
    };

    return config;
  },
  css: {
    loaderOptions: {
      sass: {
        includePaths: ['~@/scss'],
        data: '@import "@/scss/app.scss";',
      },
    },
  },
};
