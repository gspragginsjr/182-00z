modules:
  -
    type: intro
    copy: What if we made bigger strides toward a better future? At Jefferson, it’s more than a dream.
    cta: Scroll to See How
    pillars:
      -
        title: People
        color: '#24bca5'
        translate: 40%
        img:
          src: /images/introPillar__people.png
          alt: person
      -
        title: Spaces
        color: '#af208e'
        translate: 20%
        img:
          src: /images/introPillar__spaces.png
          alt: spot
      -
        title: Communities
        color: '#fcb016'
        translate: 40%
        img:
          src:  /images/introPillar__communities.png
          alt: community
      -
        title: Discovery
        color: '#4379bc'
        translate: 10%
        img:
          src: /images/introPillar__discovery.png
          alt: cell
    bottomContent:
      title: Reimagining the future starts by seeing a way forward.
      copy: By focusing on four key areas, we will change the future of Philadelphia and the world.
  -
    type: pillarsContainer
    pillars:
      -
        title: People
        color: '#24bca5'
        img:
          src: /images/pillarPeopleFade.jpg
          alt: Person
        imgBefore:
          src: /images/pillarPeopleFade__before.jpg
        intro: We are at our greatest when we think creatively and act boldly.
        description: We must start with people who seek solutions to problems that will become obvious to others 10 years from now. Only then can we begin an optimistic revolution—a paradigm shift that Reimagine will fund in clinical care, research and discovery, education, and community health.
        accordion:
          intro: 'Through Reimagine, we’re seeking significant philanthropic resources to invest even more strongly in Jefferson’s people:'
          panels:
            -
              heading: Launch the Institute for Advanced Study
              copy: We will attract a cadre of thought-leading professors and visiting scholars who will identify and explore the grand challenges of tomorrow.
            -
              heading: Establish an Honors College
              copy: We will create a select Honors College to prepare top students for 21st century professional leadership roles.
            -
              heading: Redefine undergraduate and graduate programming
              copy: Computational thinking will be our native language, which we will use to enhance our creativity core curriculum, deep global experiences, and our revolutionary Hallmark, Nexus Learning, and Co-Curricular Programming framework.
            -
              heading: Endow 30 new named professorships and departmental chairs
              copy: We will use these resources to  accelerate our academic and clinical momentum.
            -
              heading: Endow 100 scholarships and fellowships
              copy: We will empower students to think, innovate, and create change.


      -
        title: Spaces
        color: '#af208e'
        img:
          src: /images/pillarSpacesFade.jpg
          alt: building
        imgBefore:
          src: /images/pillarSpacesFade__before.jpg
        intro: Our skyline changes lifelines, as we design the facilities of the future.
        description: We can’t design the future of health and education in buildings from the past. To create the settings in which the future can unfold, we must secure and invest philanthropic resources on a bold and unprecedented scale across our enterprise.
        accordion:
          intro: 'Our facilities are more than buildings—they’re the embodiment of Jefferson’s commitment to our patients, students, faculty, clinicians, and communities. Your investment will:'
          panels:
            -
              heading: Build the hospital of the future
              copy: Our 14-story hospital of the future will have 625,000 square feet of state-of-the-art clinical space, designed for life-changing personal care.
            -
              heading: Create destination University campuses
              copy: We envision a two-campus University with inspirational educational spaces geared to focused, 21st-century professional training.
            -
              heading: Build a Biomedical Research Corridor
              copy: Located right in Center City, we are building a research corridor to provide our scientists with cutting-edge laboratories and a next-generation simulation center.
            -
              heading: Transform the patient experience
              copy: Our Specialty Care Pavilion will be a one-stop, multispecialty treatment center staffed with world-class experts.
            -
              heading: Provide exceptional care closer to home
              copy: We will continue our strategy to bring world-class Jefferson care closer to the communities we serve, making access easier and more efficient.
      -
        title: Communities
        color: '#fcb016'
        img:
          src: /images/pillarCommunitiesFade.jpg
          alt: community
        imgBefore:
          src: /images/pillarCommunitiesFade__before.jpg
        intro: Our progress is measured by Philadelphia’s, building health, hope, and opportunity.
        description: Philadelphia is the nation’s poorest and unhealthiest large city, with immense health disparities between rich and poor. Today, a baby born in Center City will live up to 20 years longer than one born in North Philadelphia. We’re committed to dismantling that inequity.
        accordion:
          intro: 'Jefferson has a long history of outreach to improve the lives of disadvantaged people in our surrounding communities. We’ve been listening to our neighbors and key ideas are already emerging:'
          panels:
            -
              heading: Philadelphia Collaborative for Health Equity
              copy: This visionary initiative harnesses the knowledge, connections, and commitment of dozens of community-based organizations.
            -
              heading: Seed-money grants
              copy: These grants enable community organizations to implement and scale up innovative programs for improving health and well-being in vulnerable areas.
            -
              heading: Partner with organizations
              copy: We will partner with local, regional, and global organizations to establish Community Health Centers in underserved areas.
            -
              heading: Partner with local nonprofits
              copy: These partnerships teach financial skills, boost food security, improve health, incentivize savings, increase employment, and reduce depression.
            -
              heading: Increase access
              copy: We will increase educational access and opportunities through scholarships.
      -
        title: Discovery
        color: '#4379bc'
        img:
          src: /images/pillarDiscoveryFade.jpg
          alt: discover
        imgBefore:
          src: /images/pillarDiscoveryFade__before.jpg
        intro: We focus on breakthroughs, investing in research, discovery, and learning.
        description: We’re developing distinctive research programs by bringing a laser focus to high-impact science and cultivating inter-institutional partnerships that enhance discovery and elevate our impact.

        accordion:
          intro: 'To better prepare the professionals, innovators, and leaders of tomorrow, we’re seeking major philanthropic investments in an array of strategic initiatives:'
          panels:
            -
              heading: Expand Medicine+
              copy: Expanding our “+” co-curricular programs will allow us to meet the challenges and opportunities of the future.
            -
              heading: Launch the Accelerator Fund
              copy: This fund will commercialize Jefferson innovations, and is sustained by gifts from benefactors, venture capitalists, investors, and industries.
            -
              heading: Cultivate novel research partnerships
              copy: These global partnerships will enhance exploration and discovery and elevate Jefferson’s profile and impact.
            -
              heading: Expand research
              copy: We will expand research in fields in which we can be a national leader, such as computational medicine and mitochondrial pathogenesis.
            -
              heading: Widen faculty access
              copy: We will expand access to core facilities and technologies by deepening on-campus research infrastructure and developing new initiatives in frontiers such as biologics engineering.
  -
    type: outro
    video:
      src: https://www.youtube.com/watch?v=k7I4n2LXCI0
    cta:
      copy: Are you in? Help us reimagine the future of Jefferson!
    donateCta:
      copy: Help us reimagine the future of Jefferson.
      button:
        text: Donate
        url: https://giving.jefferson.edu/giving/online-giving/reimagine.html?utm_source=19Q0024
  -
    type: footer
    contact: '<strong>Jefferson Office of<br>Institutional Advancement</strong><br>125 S. 9th Street, Suite 600<br>Philadelphia, PA 19107<br><br>Phone:  <a href="tel:215-955-1635">215-955-1635</a><br>Email:  <a href="mailto:giving@jefferson.edu">giving@jefferson.edu</a>'
    links:
      -
        text: Privacy
        url: https://hospitals.jefferson.edu/patients-and-visitors/patient-policies/online-privacy-statement/
      -
        text: Refund Policy
        url: https://giving.jefferson.edu/giving/about/refund-policy.html
      -
        text: Security
        url: https://giving.jefferson.edu/giving/about/data-security.html
