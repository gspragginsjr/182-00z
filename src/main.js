import Vue from 'vue';
import Glide from 'vue-glide';
import Sticky from 'vue-sticky-directive';
import VuePlyrVideo from '@160over90/vue-plyr-video';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.component('VuePlyrVideo', VuePlyrVideo);

Vue.use(Glide);
Vue.use(Sticky);

new Vue({
  render: h => h(App),
}).$mount('#app');
